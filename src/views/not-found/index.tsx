import { FC } from "react"

export const NotFound: FC = () => {
    const styles = {
        color: "red"
    }
    return (
        <div style={ styles }>
            <h1>
                404 - couldn't find the page you are looking for:
            </h1>
            { window.location.pathname }
        </div>
    )
}