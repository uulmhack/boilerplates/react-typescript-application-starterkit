import React, { FC } from "react"
import { ClickCounter } from "../../components/clicker-counter"

export const Home: FC<unknown> = () => {
    return (
        <>
            <div>
                <span>I'm the content of a span which is nested in a div</span>
                <ClickCounter initialCounterValue={ 0 }/>
                <ClickCounter initialCounterValue={ 0 } title="uulmhack demo title"/>
                <span>
                  some other label below the custom components
                </span>
            </div>
        </>
    )
}