# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.1.54](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/compare/v0.1.53...v0.1.54) (2021-05-26)

### [0.1.53](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/compare/v0.1.52...v0.1.53) (2021-05-26)


### CI

* **fix:** re-add pipeline steps from auto-release to wait for container success ([d81b982](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/commit/d81b982f34f85e313fff272414faa3a7d3122247))

### [0.1.52](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/compare/v0.1.50...v0.1.52) (2021-05-26)

### [0.1.51](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/compare/v0.1.50...v0.1.51) (2021-05-26)

### CI

* **ci-rules:** add 'workflow:rules' to .gitlab-ci.yml to prevent ([d2dd272](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/commit/d2dd27220ec7a271fa7c38726ea1913cc5568228))
### [0.1.50](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/compare/v0.1.49...v0.1.50) (2021-05-26)


### CI

* **jobs:** update includes section and remove unnecessary config ([9cf8347](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/commit/9cf8347d44727f701819cfad0e9c7a01bb6a2370))

### [0.1.49](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/compare/v0.1.48...v0.1.49) (2021-05-24)


### Docs

* **readme:** Add README file ([13965c3](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/commit/13965c31a37e0f12cdb17f9e985a7807079455ab))

### [0.1.48](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/compare/v0.1.47...v0.1.48) (2021-05-24)


### Docs

* **readme:** add description to yarn commit and yarn release section ([5333d4e](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/commit/5333d4eb5e6c03d81a8b54c27c1739eceea586c0))


### CI

* **ci-configuration:** add needs to release job, in order to only release when build also succeeds ([d8377ce](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/commit/d8377ce863f31e5af41f70ea2f7fa3e4cfabe799))

### [0.1.47](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/compare/v0.1.46...v0.1.47) (2021-05-24)


### Others

* **dockerfile:** update dockerfile to contain print when install is done ([a39900b](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/commit/a39900b57794158577020d96a543c2db75e40c51))

### [0.1.46](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/compare/v0.1.45...v0.1.46) (2021-05-23)


### Others

* **dockerfile:** add dockerfile for application ([2f9ea8d](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/commit/2f9ea8d3bc63cc24a51ffd00e92a72ad929d0a0c)), closes [#3](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/issues/3)

### [0.1.45](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/compare/v0.1.44...v0.1.45) (2021-05-23)


### CI

* **fix:** fix for pipeline duplication ([0d8174c](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/commit/0d8174cd362258162e2e19d18852a8d89c6c16ab))
* **fix:** fix pipeline ([3c12b60](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/commit/3c12b608be747f73e3c6df4a84472ad58ae34c5a))

### [0.1.44](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/compare/v0.1.43...v0.1.44) (2021-05-23)


### CI

* **add workflow:** add workflow to ci ([1120be1](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/commit/1120be14af5b8c2489dc33e6eb66e28e186f6a50))

### [0.1.43](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/compare/v0.1.42...v0.1.43) (2021-05-23)


### CI

* **fix:** fix pipeline ([de49df7](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/commit/de49df7e5fa6906b631cb194e1f997c598f55ada))

### [0.1.42](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/compare/v0.1.41...v0.1.42) (2021-05-23)


### CI

* **duplicate pipelines:** remove duplicate pipeline cretion ([828531f](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/commit/828531f9c7b7e3328532277bc29d299e187c3114))

### [0.1.41](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/compare/v0.1.40...v0.1.41) (2021-05-23)


### CI

* **fix duplicate pipelines:** fix duplicate pipelines ([9546a7a](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/commit/9546a7ae72603448531680abb2bf225d73938562))

### [0.1.40](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/compare/v0.1.39...v0.1.40) (2021-05-23)


### Features

* **app.tsx:** add router and different view components as placeholder for pages ([a5e9c57](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/commit/a5e9c57fede0541079ef0282b2579049d8f11086))

### [0.1.39](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/compare/v0.1.38...v0.1.39) (2021-05-23)


### CI

* **fix:** fix pipeline ([6afa79e](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/commit/6afa79e4d9fddd00174e4c3078b66a22b20749c8))

### [0.1.38](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/compare/v0.1.37...v0.1.38) (2021-05-23)


### CI

* **change rule:** change rule to be flexible ([602916f](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/commit/602916fa0327303f81b5f7f2cc76cb15b57e7cdb))

### [0.1.37](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/compare/v0.1.36...v0.1.37) (2021-05-23)


### CI

* **fix:** fix pipeline definition again ([247616f](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/commit/247616fa47b65691e9d2543a2234223532d3d7ce))

### [0.1.36](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/compare/v0.1.35...v0.1.36) (2021-05-23)


### CI

* **fix:** fix rule definition ([9038dc5](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/commit/9038dc5381f22049221ec6828e7c6ceada88116e))

### [0.1.35](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/compare/v0.1.34...v0.1.35) (2021-05-23)


### CI

* **fix configuration:** fix rule configuration ([8d7fa1d](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/commit/8d7fa1d2894149d6881d19da0fd12d0bdc7c379a))
* **fix rules:** fix rules for pipelinestep creation ([c81a050](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/commit/c81a050fbf69c732d0b973bc333d43c2064d447e))

### [0.1.34](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/compare/v0.1.33...v0.1.34) (2021-05-23)


### CI

* **fix ci:** f ([c90c0f4](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/commit/c90c0f4d69cf2290fb62e7cb24cc52aeddf47d42))

### [0.1.33](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/compare/v0.1.32...v0.1.33) (2021-05-23)


### CI

* **fix ci:** fix ([b8f893e](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/commit/b8f893e5a166cea84826ff8679ee2499a207b4b2))

### [0.1.32](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/compare/v0.1.31...v0.1.32) (2021-05-23)


### CI

* **add rule:** add rule to avoid duplicate pipelines ([6390e60](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/commit/6390e60efb67fce93a40a29011b5689db3f4a4cc))

### [0.1.31](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/compare/v0.1.30...v0.1.31) (2021-05-23)


### CI

* **fix:** fix ([2c206f2](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/commit/2c206f2d0f0aabd12451035c9e03d1c40e20d5c1))
* **fix:** fix ci configuration ([2701559](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/commit/2701559ae949be382c1badd19413393d299c1239))
* **fix rule:** fix rule for step generation during pipeline creation ([cfa420b](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/commit/cfa420ba924308f456f7591cfde8ff4351556ff2))
* **fix typo:** fix typo in artifact name ([ec662cc](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/commit/ec662cc5166f2b285b9411c8ccabf91b57b0b557))

### [0.1.30](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/compare/v0.1.29...v0.1.30) (2021-05-23)


### CI

* **fix configuration:** fix error with prepare step ([73dc3bd](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/commit/73dc3bd4f7358a0714f8c375c917e219b0ccbb5c))

### [0.1.29](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/compare/v0.1.28...v0.1.29) (2021-05-23)


### CI

* **refactoring:** refactor ci file and update image for prepare stage ([e89d3f7](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/commit/e89d3f7f9f5b10de2c72f02e736db26eac60c0b1))
* **release management:** add if rule for pipeline step ([2d1f331](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/commit/2d1f3313a59fd96364186a782a32e660b0e4c4ad))

### [0.1.28](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/compare/v0.1.27...v0.1.28) (2021-05-23)


### CI

* **pipeline stages:** add stages for the pipeline configuration with automatic releases ([17d4373](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/commit/17d437361d007d9f75a549e89413850ce2aeb77a))

### [0.1.27](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/compare/v0.1.26...v0.1.27) (2021-05-23)

### [0.1.26](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/compare/v0.1.25...v0.1.26) (2021-05-23)

### [0.1.25](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/compare/v0.1.24...v0.1.25) (2021-05-23)

### [0.1.24](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/compare/v0.1.23...v0.1.24) (2021-05-23)

### [0.1.23](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/compare/v0.1.22...v0.1.23) (2021-05-23)

### [0.1.22](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/compare/v0.1.21...v0.1.22) (2021-05-23)

### [0.1.21](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/compare/v0.1.20...v0.1.21) (2021-05-23)

### [0.1.20](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/compare/v0.1.19...v0.1.20) (2021-05-23)

### [0.1.19](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/compare/v0.1.18...v0.1.19) (2021-05-23)

### [0.1.18](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/compare/v0.1.17...v0.1.18) (2021-05-23)

### [0.1.17](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/compare/v0.1.16...v0.1.17) (2021-05-23)

### [0.1.16](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/compare/v0.1.15...v0.1.16) (2021-05-23)

### [0.1.15](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/compare/v0.1.14...v0.1.15) (2021-05-23)

### [0.1.14](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/compare/v0.1.13...v0.1.14) (2021-05-23)

### [0.1.13](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/compare/v0.1.12...v0.1.13) (2021-05-23)

### [0.1.12](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/compare/v0.1.10...v0.1.12) (2021-05-23)

### [0.1.11](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/compare/v0.1.10...v0.1.11) (2021-05-23)


### CI

* **release date:** add release date ([570a201](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/commit/570a2012ffdc7c6c6bef3da148ee41159ff10d43))

### [0.1.10](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/compare/v0.1.9...v0.1.10) (2021-05-23)


### CI

* **ci:** fix error ([fe5d804](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/commit/fe5d80482e13be32cc3d42cd22c518052ada36ee))
* **release timestamp:** add fixed string for release date ([8464f69](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/commit/8464f695486853bc351772325c77c989810b11a9))

### [0.1.9](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/compare/v0.1.8...v0.1.9) (2021-05-23)


### Docs

* **readme:** add hint to use yarn commit over git commit ([fd069b7](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/commit/fd069b7fcc6a98d1338328a398c1bf4a46aabeea))


### CI

* **ci variables:** add variables for timestamp, debug pipeline ([641dbf6](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/commit/641dbf69c7d1610a17be421426cd8ffab6c4fe2f))
* **release debug:** remove non-functional released_at flag in ci ([9575324](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/commit/9575324782d05fb06a98718facc8b9ac35fb3736))
* **release-configuration:** add released_at flag to gitlab release ([bb439e5](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/commit/bb439e5cf42a2b137aee138c086a063135cadfb7))

### [0.1.8](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/compare/v0.1.7...v0.1.8) (2021-05-23)


### CI

* **release stage:** remove unnecessary escape ([311be56](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/commit/311be563177fab54bdf89d2c18f4cd6741a4e6df))

### [0.1.7](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/compare/v0.1.6...v0.1.7) (2021-05-23)


### CI

* **release stage:** add git installation to release stage ([48d8b74](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/commit/48d8b748163cbd64c3378a1a8a9c38371b52d809))


### Docs

* **readme:** add explanation for yarn commit script ([a715e7a](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/commit/a715e7a627628e974c5a89333dc01c87b88e4e9d))

### [0.1.6](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/compare/v0.1.5...v0.1.6) (2021-05-23)


### CI

* **automatic-releases:** add changelog diff generation in release step ([384f466](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/commit/384f466d1b34c6b5f44951c3d2fd22a4c25cd3ce))


### Docs

* **readme:** add heading for terminology section and project structure ([8f2e5a9](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/commit/8f2e5a90f61a4ce7bc96c0281d6afc7199a53c22))
* **readme:** add one line of content about terminology and structure of the project ([01039d5](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/commit/01039d588a0d4b828d40584528ff87db4ca074ae))

### [0.1.5](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/compare/v0.1.4...v0.1.5) (2021-05-23)


### CI

* **ci-automatic-releases:** add automatic release description generation ([11f5035](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/commit/11f50358e78de87246dc9077b395e794174b5c16))

### [0.1.4](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/compare/v0.1.3...v0.1.4) (2021-05-23)


### CI

* **ci-configuration:** add git installation to ci step ([bef095a](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/commit/bef095ae5f5f3586a3aaffa9351a67ecdb20d80e))
* **ci-configuration:** prepare release ([b40d4ab](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/commit/b40d4ab06b45274089664bb3b3c075710a0bb14c))
* **ci-configuration:** prepare release ([8db0e07](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/commit/8db0e07a09de9dea3a03538b4c77c7441076c5c4))
* **ci-configuration:** prepare release ([bd90058](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/commit/bd90058d2c139b9b7177270c3bc8b9879c5d8512))
* **ci-configuration:** prepare release ([8046e71](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/commit/8046e71620d9aff17b563531197b0380ad6ae93d))
* **ci-configuration:** prepare release ([4599fe2](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/commit/4599fe272c5a4cabf11f728b1b8f9b0d8708e892))
* **ci-configuration:** prepare release ([25eaa6c](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/commit/25eaa6c22030dec06a0ac7111915a5dfdff9ec10))
* **ci-configuration:** prepare release ([bec6399](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/commit/bec6399357a1b34fb13cac06ebf66fe6716e4265))
* **ci-configuration:** prepare release ([e202e51](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/commit/e202e51ef38a8a60372981ed3d6074186ca1714d))
* **ci-configuration:** prepare release ([5b9984c](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/commit/5b9984ca97a3bd528abd8a357a5b9c255cedf126))
* **ci-configuration:** prepare release ([86e4649](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/commit/86e46494808566b2404fdbce81a8a82210cad57f))
* **ci-configuration:** prepare release ([fba7545](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/commit/fba7545b0ca919c509829fdb011d01f4c8aa3139))

### [0.1.3](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/compare/v0.1.2...v0.1.3) (2021-05-22)


### Docs

* **readme:** prepare release ([5df94e1](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/commit/5df94e1bc4dffb69a877444050bcf3ebe9eabaad))


### CI

* **ci-configuration:** add automatic release to CI pipeline ([cebfe56](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/commit/cebfe56be4ca7d72c05ae5b77f2fe40dadd9a07b))
* **ci-configuration:** add default image to ci file ([85a9af7](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/commit/85a9af79ce07e31f5ae5d1a58a823f61a7d20efc))
* **ci-configuration:** add missing script section to release step ([ae8667d](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/commit/ae8667d1d7cc3e9722a8df0e87efff001eb26a80))
* **ci-configuration:** remove erroneous released_at key ([09c1523](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/commit/09c1523578eb41c17d2022bffd67e0e6889e7087))

### [0.1.2](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/compare/v0.1.1...v0.1.2) (2021-05-22)


### Docs

* **readme:** add section about commit script to readme ([839eb35](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/commit/839eb35c2e3e985d60a776ff55177025e1380d02))


### Code Refactoring

* **app.tsx:** add a meaningless label to Application ([0f42e1c](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/commit/0f42e1c94bf6fa27479b17673fda66aff2653add))

### 0.1.1 (2021-05-22)


### Others

* **project:** add commit linting and automatic changelog generation ([212d336](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/commit/212d3367207cc8cc1f4a2ebf87f266f53d1b6784))
