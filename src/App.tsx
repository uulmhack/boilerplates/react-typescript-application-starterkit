import React from 'react';
import {
  BrowserRouter as Router,
  Route,
  Switch 
} from "react-router-dom";
import './App.scss';
import { Header } from "./components/header"
import { Blog } from './views/blog';
import { Home } from './views/home';
import { NotFound } from './views/not-found';

export const App = () => {

  return (
    <div className="App">
      <Header heading="Hello React World"/>
      <Router>
        <Switch>
          <Route exact path="/" component={ Home }/>
          <Route exact path="/blog" component={ Blog }/>
          <Route path="" component={ NotFound }/>
        </Switch>
      </Router>
    </div>
  );
}

