import React, { FC, useState } from "react"
import { IClickCounterProps } from "./interface"
import "./clickerCounter.scss"
export const ClickCounter: FC<IClickCounterProps> = (props: IClickCounterProps) => {
    const {
        title,
        initialCounterValue
    } = props
    const [counterValue, setCounterValue] = useState<number>(initialCounterValue)
    return (
        <div className="ClickerCounterBox">
            <h4>
                { /* someNullishValue ?? <Default to use> */ }
                { title ?? "This is default"}
            </h4>
            <span className="CounterLabel">
                This is the label which shows a counter: { counterValue }
            </span>
            <button className="ClickerCounterButton" onClick={ () => setCounterValue(counterValue + 1) }>
                Smash me for number increase
            </button>
            <button className="ClickerCounterButton" onClick={ () => setCounterValue(0) }>
                Reset Counter!
            </button>
        </div>
    )    
}