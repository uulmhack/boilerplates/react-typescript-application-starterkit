# React Typescript Application Starterkit

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

[[_TOC_]]

## Terminology

This section covers the Terminology used in this starterkit application and provides information about the React Environment and Terminology.

| Term | Explanation |
|:---:|:---|
| `project root` | The top-level folder which contains all project related data is called `project (root)`. Imagine your project is called "Project-X" and all data is stored in a folder `project-x`, then this folder is your `root`. |
<hr>

## Project Structure

The following part will give an overview of the Project Structure and shortly tries to explain how the code is organized and why. In each expandable section there will be a few words alongside explanations according to the current section.

<details>
	<summary>
		<strong>
			About the project root
		</strong>
	</summary>
	<p style="border:1px solid lightgrey;border-radius:0.5rem;padding: 0.25rem 1rem;">
		<a href=".">.</a><br>
		├── <a href="./docs/">docs</a><br>
		├── <a href="./public/">public</a><br>
		├── <a href="./src/">src</a><br>
		├── <a href="./CHANGELOG.md">CHANGELOG.md</a><br>
		├── <a href="./Dockerfile">Dockerfile</a><br>
		├── <a href="./package.json">package.json</a><br>
		├── <a href="./README.md">README.md</a><br>
		├── <a href="./tsconfig.json">tsconfig.json</a><br>
		├── <a href="./yarn-error.log">yarn-error.log</a><br>
		└── <a href="./yarn.lock">yarn.lock</a><br>
		<br>
	</p>

This section will be extended in the future. An issue for this already exists (#5)

> The top-level folder which contains all project related data is called the `project (root)`.

In the `root` of a `react` project there are many different files.
The most important file in a `node` project is the `package.json` file since most parts of the project configuration is stored here, as well as the `version` of the project, its `dependencies` and `scripts`.
If the project is a `TypeScript` based `node` project there will also be a `tsconfig.json`, where (for example) the compiler configuration for your typescript project is stored.

</details>

<details>
	<summary>
		<strong>
			The contents of `src`
		</strong>
	</summary>
	<div style="display:flex; flex-direction:row;">
		<p style="border:1px solid lightgrey;border-radius:0.5rem;padding: 0.25rem 1rem;">
			<a href="./src">./src</a><br>
			├── <a href="./src/components/">components</a><br>
			│   ├── <a href="./src/components/clicker-counter/">clicker-counter</a><br>
			│   └── <a href="./src/components/header/">header</a><br>
			├── <a href="./src/views/">views</a><br>
			│   ├── <a href="./src/views/blog/">blog</a><br>
			│   ├── <a href="./src/views/home/">home</a><br>
			│   └── <a href="./src/views/not-found/">not-found</a><br>
			├── <a href="./src/App.css">App.css</a><br>
			├── <a href="./src/App.test.tsx">App.test.tsx</a><br>
			├── <a href="./src/App.tsx">App.tsx</a><br>
			├── <a href="./src/index.css">index.css</a><br>
			├── <a href="./src/index.tsx">index.tsx</a><br>
			├── <a href="./src/logo.svg">logo.svg</a><br>
			├── <a href="./src/react-app-env.d.ts">react-app-env.d.ts</a><br>
			├── <a href="./src/reportWebVitals.ts">reportWebVitals.ts</a><br>
			└── <a href="./src/setupTests.ts">setupTests.ts</a><br>
			<br><br>
		</p>
		<p style="display:flex;align-self:center;margin-left:2.5rem;padding:1rem;">
			This section will be extended in the future. An issue for this already exists (#4)
		</p>
	</div>
	<details>
		<summary>
			<strong>
				The contents of `components`
			</strong>
		</summary>
		<div style="display:flex; flex-direction:row;">
			<p style="border:1px solid lightgrey;border-radius:0.5rem;padding: 0.25rem 1rem;">
				<a href="./src/components">./src/components</a><br>
				├── <a href="./src/components/clicker-counter/">clicker-counter</a><br>
				└── <a href="./src/components/header/">header</a><br>
				<br><br>
			</p>
			<p style="display:flex;align-self:center;margin-left:2.5rem;padding:1rem;">
				This section will be extended in the future. An issue for this already exists (#6)
			</p>
		</div>
	</details>
</details>
<hr>

## Available Scripts

In the project directory, you can run different scripts, which are listed below. For further information about a script expand the corresponding section:

<details>
    <summary markdown="span">
        <strong>yarn release<strong>
    </summary>

    USE THIS COMMAND WITH CAUTION

Runs the automatic Changelog-generation and commits the result with a tag that is created based on previous commits contents.
This script should only be executed by a Project **Maintainer** (someone how knows what he/she is doing there).
</details>

<details>
    <summary markdown="span">
        <strong>
            yarn commit
        </strong>
    </summary>

    This should be used instead of `git commit` in order to make use of conventional changelog generation

Runs the commitizen cli and guides you through the commit in order to have standardized commit messages and are able to automatically generate changelogs from commits.

**Note:** This script should replace `git commit` in order to keep the commit history clean and in a consistent format.
</details>

<details>
    <summary markdown="span">
        <strong>
            yarn start
        </strong>
    </summary>
Runs the app in the development mode.

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.
</details>

<details>
<summary markdown="span">
    <strong>
        yarn test
    </strong>
</summary>

Launches the test runner in the interactive watch mode.
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.
</details>

<details>
    <summary markdown="span">
        <strong>
            yarn build
        </strong>
    </summary>
Builds the app for production to the `build` folder.
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.
</details>

<details>
    <summary markdown="span">
        <strong>
            yarn eject
        </strong>
    </summary>

    Note: this is a one-way operation. Once you `eject`, you can’t go back!

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.
</details>
<hr>

## Learn More about Create-React-App (CRA)

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).
