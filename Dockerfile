FROM node:14.7-alpine3.12

WORKDIR /app

ADD . /app

EXPOSE 3000

RUN yarn install && echo "Installed all dependencies"

CMD ["yarn", "start"]